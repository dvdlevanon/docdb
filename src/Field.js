"use strict";

class Field {
	constructor(name, type, referencedDocument) {
		if (!name) {
			console.log("DocDB: No name provided for Field");
		}
		
		if (!type) {
			console.log("DocDB: No type provided for Field");
		}
		
		// Don't check for referencedDocument because it may be set later for 
		//	documents that references themselves
		
		this.name = name;
		this.type = type;
		this.referencedDocument = referencedDocument;
	}
}

module.exports = Field;
