"use strict";

var Serializer 	= require("../Serializer.js")

class DBMap {
	constructor(db, name) {
		this.db = db;
		this.name = "map/" + name + "/";
	}
	
	put(key, value, callback) {
		var operations = [];
		this.putBatch(key, value, operations);
		
		this.db.batch(operations, function(err) {
			callback(err);
		});
	}
	
	putBatch(key, value, operations) {
		operations.push({ type: "put", key: this.keyToBuffer(key), value: value });
	}
	
	delete(key, callback) {
		var operations = [];
		this.deleteBatch(key, operations);
		
		this.db.batch(operations, function(err) {
			callback(err);
		});
	}
	
	deleteBatch(key, operations) {
		operations.push({ type: "del", key: this.keyToBuffer(key) });
	}
	
	exists(key, callback) {
		var key = this.keyToBuffer(key);
		
		this.db.get(key, function(err, value) {
			callback(err, value ? true : false);
		});
	}
	
	list(callback) {
		var keyTransformer = function(buffer) {
			return this.bufferToKey(buffer);
		}.bind(this);
		
		this.db.keys(this.firstKey(), this.lastKey(), function(err, entries) {
			callback(err, entries);
		}, keyTransformer);
	}
	
	firstKey() {
		var key = Buffer.alloc(this.name.length + 1);
		key.write(this.name);
		key.writeInt8(0, this.name.length);
		return key;
	}
	
	lastKey() {
		var key = Buffer.alloc(this.name.length + 1);
		key.write(this.name);
		key.writeInt8(1, this.name.length);
		return key;
	}
	
	keyToBuffer(key) {
		return Buffer.concat([
			this.firstKey(),
			Serializer.serialize(key)
		]);
	}
	
	bufferToKey(buffer) {
		return Serializer.deserialize(
			Buffer.from(buffer).slice(
				this.firstKey().length));
	}
}

module.exports = DBMap;
