"use strict";

var Serializer 	= require("../Serializer.js")
var Async 		= require("async");

class DBSet {
	constructor(db, name) {
		this.db = db;
		this.rawName = name;
		this.name = "set/" + name + "/";
	}
	
	put(value, callback) {
		var operations = [];
		this.putBatch(value, operations);
		
		this.db.batch(operations, function(err) {
			callback(err);
		});
	}
	
	putBatch(value, operations) {
		operations.push({ type: "put", key: this.valueToKey(value), value: value });
	}
	
	delete(value, callback) {
		var operations = [];
		this.deleteBatch(value, operations);
		
		this.db.batch(operations, function(err) {
			callback(err);
		});
	}
	
	deleteBatch(value, operations) {
		operations.push({ type: "del", key: this.valueToKey(value) });
	}
	
	clear(callback) {
		var set = this;
		
		Async.waterfall([
			function createClearBatch(callback) {
				var operations = [];

				set.db.forEachValue(set.firstKey(), set.lastKey(), 
					function onData(data) {
						set.deleteBatch(data, operations);
						return true;
					},
					function done(err) {
						callback(err, operations);
					});
			}
		],
		function runBatch(err, operations) {
			if (err) {
				return callback(err);
			}
			
			set.db.batch(operations, function(err) {
				callback(err);
			});
		});
	}

	exists(value, callback) {
		var key = this.valueToKey(value);
		
		this.db.get(key, function(err, value) {
			callback(err, value ? true : false);
		});
	}
	
	list(callback) {
		this.db.values(this.firstKey(), this.lastKey(), function(err, values) {
			callback(err, values);
		});
	}
	
	firstKey() {
		var key = Buffer.alloc(this.name.length + 1);
		key.write(this.name);
		key.writeInt8(0, this.name.length);
		return key;
	}
	
	lastKey() {
		var key = Buffer.alloc(this.name.length + 1);
		key.write(this.name);
		key.writeInt8(1, this.name.length);
		return key;
	}
	
	valueToKey(value) {
		return Buffer.concat([
			this.firstKey(),
			Serializer.serialize(value)
		]);
	}
}

module.exports = DBSet;
