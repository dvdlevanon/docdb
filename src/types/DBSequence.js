"use strict";

var Async = require("async");
var Locks = require('locks');

class DBSequence {
	constructor(db, name) {
		this.db = db;
		this.name = "seq/" + name;
		this.lock = Locks.createMutex();
	}
	
	next(doneCallback) {
		var sequence = this;
		
		Async.waterfall([
			function lock(callback) {
				sequence.lock.lock(function() {
					callback();
				});
			},
			function getValue(callback) {
				sequence.db.get(sequence.name, function(err, value) {
					if (err) {
						callback(err);
						return;
					}
					
					callback(null, value || 0);
				});
			},
			function incrementValue(value, callback) {
				sequence.db.put(sequence.name, ++value, function(err) {
					callback(err, value);
				});
			}
		],
		function done(err, value) {
			sequence.lock.unlock();
			doneCallback(err, value);
		});
	}
}

module.exports = DBSequence;
