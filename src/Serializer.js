"use strict";

var BufferSerializer = require("buffer-serializer");

class Serializer {
	static serialize(value) {
		if (!this.serializer) {
			this.serializer = new BufferSerializer();
		}
		
		return this.serializer.toBuffer(value);
	}
	
	static deserialize(value) {
		if (!this.serializer) {
			this.serializer = new BufferSerializer();
		}
		
		return this.serializer.fromBuffer(value);
	}
}

module.exports = Serializer;
