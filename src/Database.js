"use strict";

var level 		= require("level")
var Stringify 	= require("node-stringify");
var LevelCodec	= require("level-codec");
var Serializer 	= require("./Serializer.js")

class Database {
	constructor(path) {
		var options = {
			valueEncoding: {
				encode: function (value) {
					return Serializer.serialize(value);
				},
				decode: function (value) {
					return Serializer.deserialize(value);
				},
				buffer: true,
				type: "serializer"
			},
			keyEncoding: {
				encode: function (value) {
					return value;
				},
				decode: function (value) {
					return value;
				},
				buffer: true,
				type: "serializer"
			}
		};
		
		this.db = level(path, options)
	}
	
	put(key, value, callback) {
		this.db.put(key, value, function(err) {
			callback(err);
		});
	}
	
	del(key, callback) {
		this.db.del(key, function(err) {
			callback(err);
		});
	};
	
	get(key, callback) {
		this.db.get(key, function(err, value) {
			if (err && err.type == "NotFoundError") {
				err = null;
			}
			
			callback(err, value);
		});
	}
	
	batch(operations, callback) {
		this.db.batch(operations, function (err) {
			callback(err);
		});
	}
	
	keys(start, end, callback, keyTransformer) {
		var options = {
			gte: start,
			lte: end
		}
		
		var result = {};
		
		this.db.createReadStream(options)
			.on("data", function(data) {
				var key = data.key;
				
				if (keyTransformer) {
					key = keyTransformer(key)
				}

				result[key] = data.value;
			})
			.on("error", function(err) {
				callback(err);
			})
			.on("end", function() {
				callback(null, result);
			});
	}
	
	forEachValue(start, end, callback, doneCallback) {
		var options = {
			gte: start,
			lte: end,
			keys: false
		}
		
		var stop = false;
		
		this.db.createReadStream(options)
			.on("data", function(data) {
				if (stop) { 
					return; 
				}
				
				stop = !(callback(data));
				
				if (stop) {
					doneCallback(null);
				}
			})
			.on("error", function(err) {
				doneCallback(err);
			})
			.on("end", function() {
				doneCallback(null);
			});
	}
	
	values(start, end, callback) {
		var result = [];
		
		this.forEachValue(start, end, 
			function onData(data) {
				result.push(data);
				return true;
			}, 
			function done(err) {
				callback(err, result);
			});
	}
}

module.exports = Database;
