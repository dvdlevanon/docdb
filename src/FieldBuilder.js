"use strict";

var Field = require("./Field");

class FieldBuilder {
	constructor() {
		this.name = null;
		this.type = "scalar";
		this.referencedDocument = null;
	}
	
	extends(parent) {
		this.name = parent.name;
		this.type = parent.type;
		this.referencedDocument = parent.referencedDocument;
		return this;
	}
	
	setName(name) {
		this.name = name;
		return this;
	}
	
	setType(type) {
		this.type = type;
		return this;
	}
	
	setReference(referencedDocument) {
		this.setType("reference");
		this.referencedDocument = referencedDocument;
		return this;
	}
	
	build() {
		return new Field(this.name, this.type, this.referencedDocument);
	}
}

module.exports = FieldBuilder;
