"use strict";

var Async 		= require("async");
var Locks 		= require('locks');
var DBSequence 	= require("./types/DBSequence.js")
var DBSet 		= require("./types/DBSet.js")
var DBMap 		= require("./types/DBMap.js")

class Document {
	constructor(db, name, fields) {
		this.db = db;
		this.name = name;
		this.fields = fields;
		this.idSequence = new DBSequence(this.db, "docs/" + this.name + "/id");
		this.ids = new DBSet(this.db, "docs/" + this.name + "/ids");
	}
	
	save(doc, doneCallback) {
		var document = this;
		
		Async.waterfall([
			function generateId(callback) {
				if (!doc.id) {
					document.idSequence.next(function(err, id) {
						callback(err, id);
					});
				} else {
					callback(null, doc.id);
				}
			},
			function saveDocument(id, callback) {
				var result = document.createSaveBatch(id, doc);
				var operations = result.operations;
				var setsToRemove = result.setsToRemove;
				
				Async.parallel([
					function runBatch(callback) {
						document.db.batch(operations, function(err) {
							callback(err);
						});
					},
					function removeSets(callback) {
						Async.eachOfLimit(setsToRemove, 5, function(set, key, callback) {
							set.clear(function(err) {
								callback(err);
							});
						},
						function doneRemoveSets(err) {
							callback(err);
						});
					}
				],
				function done(err) {
					callback(err, id);
				});
			}
		],
		function done(err, id) {
			doneCallback(err, id);
		});
	}
	
	createSaveBatch(id, doc) {
		var setsToRemove = {};
		var operations = [];
		var result = {
			setsToRemove: setsToRemove,
			operations: operations
		};
		
		var values = new DBMap(this.db, "docs/" + this.name + "/" + id + "/values")
		
		this.ids.putBatch(id, operations);
		
		for (var fieldName in this.fields) {
			var field = this.fields[fieldName];
			var value = null;
			
			if (field.type == "scalar") {
				value = doc[field.name];
			} else if (field.type == "set") {
				var array = doc[field.name];	

				if (Array.isArray(array)) {
					var set = this.getSetField(id, field.name);
					value = set.rawName;
					
					for (var i = 0; i < array.length; i++) {
						set.putBatch(array[i], operations);
					}
				} else {
					var set = this.getSetField(id, field.name);
					setsToRemove[field.name] = set;
				}
			} else if (field.type == "reference") {
				value = doc[field.name];
			}
			
			if (value) {
				values.putBatch(field.name, value, operations);
			} else {
				values.deleteBatch(field.name, operations);
			}
		}
		
		return result;
	}
	
	load(id, options, doneCallback) {
		if (!doneCallback) {
			doneCallback = options;
			options = {};
		}
		
		var document = this;
		var values = new DBMap(this.db, "docs/" + this.name + "/" + id + "/values")
		
		Async.waterfall([
			function loadDocument(callback) {
				values.list(function(err, keys) {
					if (err) {
						return callback(err);
					}

					var result = document.createFromKeys(id, keys);
					var doc = result.doc;
					var sets = result.sets;
					var references = result.references;

					callback(null, sets, references, doc)
				});
			},
			function loadSets(sets, references, doc, callback) {
				document.loadSets(doc, sets, references, function(err) {
					if (err) {
						return callback("error loading sets: " + err);
					}
					
					callback(null, references, doc);
				});
			},
			function loadReferences(references, doc, callback) {
				if (!options.referencesDepth || options.referencesDepth == 0) {
					return callback(null, doc);
				}
				
				var newOptions = Object.assign(options, {
					referencesDepth: (options.referencesDepth - 1)
				});

				Async.eachOfLimit(references, 5, function(referencedDocument, fieldName, callback) {
					var id = doc[fieldName];
					var field = document.fields[fieldName];
					
					if (field.type == "set") {
						var arrayOfIds = id;
						doc[fieldName] = { };
						
						Async.eachOfLimit(arrayOfIds, 5, function(id, index, callback) {
							field.referencedDocument.load(id, newOptions, function(err, loadedDocuement) {
								if (err) {
									return callback("error loading reference of set: " + err);
								}
								
								doc[fieldName][loadedDocuement.id] = loadedDocuement;
								callback(null);
							});
						},
						function done(err) {
							callback(err);
						});
					} else {
						field.referencedDocument.load(id, newOptions, function(err, loadedDocuement) {
							if (err) {
								return callback("error loading reference: " + err);
							}
							
							doc[fieldName] = loadedDocuement;
							callback(null);
						});
					}
				},
				function done(err) {
					return callback(err, doc);
				});
			}
		],
		function done(err, doc) {
			return doneCallback(err, doc);
		});
	}
	
	loadSets(doc, sets, references, callback) {
		var document = this;
		
		Async.eachOfLimit(sets, 5, function(set, key, callback) {
			var field = document.fields[key];

			if (field.referencedDocument) {
				references[key] = field.referencedDocument;
			}

			set.list(function(err, values) {
				if (err) {
					callback(err);
					return;
				}

				doc[key] = values;
				callback(null);
			});
		}, 
		function done(err) {
			callback(err);
		});
	}
	
	createFromKeys(id, keys) {
		var doc = { id:id };
		var sets = { };
		var references = { };
		
		var result = { 
			doc: doc,
			sets: sets,
			references: references
		}

		for (var key in keys) {
			var field = this.fields[key];

			if (!field) {
				continue;
			}
			
			var value = keys[key];
			
			if (value && field.type == "set") {
				var set = new DBSet(this.db, value);
				sets[key] = set;
			} else if (value && field.type == "reference") {
				var referencedDocument = field.referencedDocument;
				references[key] = referencedDocument;
			}
			
			doc[key] = value;
		}
		
		return result;
	}
	
	listIds(doneCallback) {
		this.ids.list(doneCallback);
	}
	
	loadAllDocuments(options, doneCallback) {
		if (!doneCallback) {
			doneCallback = options;
			options = {};
		}
		
		var document = this;
		
		Async.waterfall([
			function listIds(callback) {
				document.listIds(function(err, ids) {
					callback(err, ids)
				});
			},
			function load(ids, callback) {
				var documents = [];
				
				Async.each(ids, function done(id, callback) {
					document.load(id, Object.assign({}, options), function(err, loadedDocuement) {
						documents.push(loadedDocuement);
						callback(err);
					});
				},
				function done(err) {
					callback(err, documents)
				});
			}
		],
		function done(err, documents) {
			doneCallback(err, documents);
		});
	}
	
	getSetField(documentId, setName) {
		var field = this.fields[setName];
		
		if (!field) {
			return false;
		}
		
		return new DBSet(this.db, "docs/" + this.name + "/" + documentId + "/" + field.name);
	}
	
	setPut(documentId, setName, value, callback) {
		var set = this.getSetField(documentId, setName);
		
		if (!set) {
			return callback("Field not found " + setName);
		}

		set.put(value, function(err) {
			callback(err);
		});
	}
	
	setDelete(documentId, setName, value, callback) {
		var set = this.getSetField(documentId, setName);
		
		if (!set) {
			return callback("Field not found " + setName);
		}
		
		set.delete(value, function(err) {
			callback(err);
		});
	}
	
	setReferencedDocument(fieldName, referencedDocument) {
		this.fields[fieldName].referencedDocument = referencedDocument;
	}
}

module.exports = Document;
