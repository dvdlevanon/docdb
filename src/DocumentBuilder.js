"use strict";

var Document = require("./Document");
var Field = require("./Field");
var FieldBuilder = require("./FieldBuilder");

class DocumentBuilder {
	constructor() {
		this.db = null;
		this.name = null;
		this.fields = {};
	}
	
	setDB(db) {
		this.db = db;
		return this;
	}
	
	setName(name) {
		this.name = name;
		return this;
	}
	
	extends(parent) {
		this.db = parent.db;
		this.name = parent.name;

		for (var parentFieldName in parent.fields) {
			var parentField = parent.fields[parentFieldName];
						
			this.fields[parentField.name] = parentField;
		}
		
		return this;
	}
	
	addField(fieldBuilder) {
		var field = fieldBuilder.build();
		this.fields[field.name] = field;
		return this;
	}
	
	build() {
		return new Document(this.db, this.name, this.fields)
	}
}

module.exports = DocumentBuilder;
