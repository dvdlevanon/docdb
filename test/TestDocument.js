var Should 			= require("should");
var FileSystem 		= require("fs-extra");
var OS 				= require("os");
var Path			= require("path");
var Async 			= require("async");
var Database 		= require("../src/Database");
var Document 		= require("../src/Document");
var DocumentBuilder	= require("../src/DocumentBuilder");
var FieldBuilder	= require("../src/FieldBuilder");

describe("Document", function() {
	var dbpath = Path.join(OS.tmpdir(), "docdb-test/test-db-doc");
	FileSystem.emptyDirSync(dbpath);
	var db = new Database(dbpath);
	
	var persons = new DocumentBuilder()
		.setDB(db)
		.setName("person")
		.addField(new FieldBuilder()
			.setName("name"))
		.addField(new FieldBuilder()
			.setName("address"))
		.addField(new FieldBuilder()
			.setName("phone"))
		.build();
	
	describe("document.save(doc)", function() {
		var personId;
		
		it("Saving new Document", function(done) {
			var person = {
				name: "david",
				address: "some address",
				phone: "123-4567890"
			}
			
			persons.save(person, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				personId = result;
				done();
			});
		});
		
		it("Updating document", function(done) {
			var person = {
				id: personId,
				name: "david",
				address: "some address"
			}
			
			persons.save(person, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				result.should.equal(personId);
				
				persons.load(person.id, function(err, result) {
					Should.not.exist(err);
					Should.exist(result);
					Should.not.exist(result.phone);
					Should.exist(result.name);
					Should.exist(result.address);
					person.id.should.equal(result.id);
					done();
				});
			});
		});
	});
	
	describe("document.listIds()", function() {
		it("List existing documents", function(done) {
			persons.listIds(function(err, ids) {
				Should.not.exist(err);
				Should.exist(ids);
				ids.should.have.length(1);
				done();
			});
		});
	});
	
	describe("document.load(id)", function() {
		it("Load document", function(done) {
			var person = {
				name: "david",
				address: "some address",
				phone: "123-4567890"
			}
			
			persons.save(person, function(err, personId) {
				Should.not.exist(err);
				Should.exist(personId);
				person.id = personId;
				
				persons.load(personId, function(err, result) {
					Should.not.exist(err);
					Should.exist(personId);
					
					result.should.deepEqual(person);
					done();
				});
			});
		});
	});

	describe("document.loadAllDocuments", function() {
		var motorcycles = new DocumentBuilder()
			.setDB(db)
			.setName("motorcycles")
			.addField(new FieldBuilder()
				.setName("name"))
			.addField(new FieldBuilder()
				.setName("price"))
			.build();
		
		it("Load all documents", function(done) {
			var motors = [
				{ name: "r1", price: "12000" },
				{ name: "cbr", price: "13000" },
				{ name: "vfr", price: "14000" }
			];
			
			Async.each(motors,
				function save(entry, callback) {
					motorcycles.save(entry, function(err, id) {
						Should.not.exist(err);
						Should.exist(id);
						entry.id = id;
						callback(err);
					});
				},
				function finish(err) {
					Should.not.exist(err);
					
					motorcycles.loadAllDocuments(function(err, docs) {
						Should.not.exist(err);
						docs.should.have.length(3);
						
						for (var i = 0; i < docs.length; i++) {
							motors.should.containEql(docs[i]);
						}
					
						docs.should.be.eql(motors);
						done();
					});
				}
			);
		});
	});
	
	describe("document.reference", function() {
		var men = new DocumentBuilder()
			.setDB(db)
			.setName("men")
			.addField(new FieldBuilder()
				.setName("id"))
			.addField(new FieldBuilder()
				.setName("name"))
			.addField(new FieldBuilder()
				.setName("parent")
				.setReference("reference"))
			.build();
			
		men.setReferencedDocument("parent", men);
			
		var father = { id:999, name:"father" }
		var son = { name:"son", parent:999 }
		
		it("Save father and son", function(done) {
			men.save(father, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				
				men.save(son, function(err, result) {
					Should.not.exist(err);
					Should.exist(result);
					son.id = result;
					done();
				});
			});
		});
		
		it("Load son", function(done) {
			men.load(son.id, { referencesDepth:1 }, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				result.parent.should.eql(father);
				done();
			});
		});
	});
	
	describe("document.sets", function() {
		var managers = new DocumentBuilder()
			.setDB(db)
			.setName("manager")
			.addField(new FieldBuilder()
				.setName("name"))
			.addField(new FieldBuilder()
				.setName("workers")
				.setType("set"))
			.build();
			
		var manager = {
			name: "the manager",
			phone: "343434343443243243",
			workers: [
				"workerId1",
				"workerId2",
				"workerId3",
				"workerId4",
				"workerId5"
			]
		}
		
		it("Save manager with workers", function(done) {
			managers.save(manager, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				manager.id = result;
				done();
			});
		});
		
		it("Loading manager", function done(done) {
			managers.load(manager.id, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				Should.exist(result.name);
				manager.id.should.equal(result.id);
				manager.workers.should.eql(result.workers);
				done();
			});
		});
		
		it("Save manager without workers", function(done) {
			manager.workers = null;
			managers.save(manager, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				manager.id = result;
				done();
			});
		});
		
		it("Check workers removed", function done(done) {
			managers.load(manager.id, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				Should.not.exist(result.workers);
				Should.exist(result.name);
				manager.id.should.equal(result.id);
				done();
			});
		});
		
		it("Save manager again with 2 workers", function(done) {
			manager.workers = ["workerId1", "workerId2"];
			
			managers.save(manager, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				manager.id.should.equal(result);
				done();
			});
		});
		
		it("Check there are only 2 workers", function done(done) {
			managers.load(manager.id, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				Should.not.exist(result.address);
				Should.exist(result.name);
				result.id.should.equal(manager.id);
				result.workers.should.eql(manager.workers);
				done();
			});
		});
		
		it("Check setPut", function done(done) {
			manager.workers.push("david");
			
			managers.setPut(manager.id, "workers", "david", function(err) {
				Should.not.exist(err);
				
				managers.load(manager.id, function(err, result) {
					Should.not.exist(err);
					
					result.workers.length.should.equal(3);
					
					for (var i = 0; i < result.workers.length; i++) {
						manager.workers.should.containEql(result.workers[i]);
					}
					
					done();
				});
			});
		});
		
		it("Check setDelete", function done(done) {
			manager.workers.splice(manager.workers.indexOf("david"), 1);
			
			managers.setDelete(manager.id, "workers", "david", function(err) {
				Should.not.exist(err);
				
				managers.load(manager.id, function(err, result) {
					Should.not.exist(err);
					
					result.workers.length.should.equal(2);
					
					for (var i = 0; i < result.workers.length; i++) {
						manager.workers.should.containEql(result.workers[i]);
					}
					
					done();
				});
			});
		});
	});
	
	describe("document.inheritance", function() {
		var employeeId;
		
		var employees = new DocumentBuilder()
			.setDB(db)
			.setName("employee")
			.extends(persons)
			.addField(new FieldBuilder()
				.setName("department"))
			.build();
		
		it("Save employee", function(done) {
			var employee = {
				name: "ron",
				address: "some address",
				phone: "123-4567890",
				department: "economy"
			}
			
			employees.save(employee, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				employeeId = result;
				done();
			});
		});
		
		it("Updating employee", function(done) {
			var employee = {
				id: employeeId,
				name: "david",
				address: "some address",
				department: "red"
			}
			
			employees.save(employee, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				result.should.equal(employeeId);
				
				employees.load(employee.id, function(err, result) {
					Should.not.exist(err);
					Should.exist(result);
					Should.not.exist(result.phone);
					Should.exist(result.name);
					Should.exist(result.address);
					Should.exist(result.department);
					employee.id.should.equal(result.id);
					employee.department.should.equal(result.department);
					done();
				});
			});
		});
	});
	
	describe("Set of references", function() {
		var tags = new DocumentBuilder()
			.setDB(db)
			.setName("tag")
			.addField(new FieldBuilder()
				.setName("id"))
			.addField(new FieldBuilder()
				.setName("name"))
			.addField(new FieldBuilder()
				.setName("items")
				.setType("set"))
			.build();
						
		var items = new DocumentBuilder()
			.setDB(db)
			.setName("item")
			.addField(new FieldBuilder()
				.setName("id"))
			.addField(new FieldBuilder()
				.setName("name"))
			.addField(new FieldBuilder()
				.setName("tags")
				.setType("set"))
			.build();
			
		tags.setReferencedDocument("items", items);
		items.setReferencedDocument("tags", tags);
		
		var drama = { id:1, name:"drama", items:[]};
		var horor = { id:2, name:"horor", items:[]};
		var comedy = { id:3, name:"comedy", items:[]};
		
		var theBigLebowski = { id:1, name:"the big lebowski", tags:[ comedy.id ]};
		var euroTrip = { id:2, name:"euro trip", tags:[ horor.id, comedy.id ]};
		var theGladiator = { id:3, name:"the gladiator", tags:[ drama.id, horor.id, comedy.id ]};
		
		drama.items.push(theGladiator.id);
		horor.items.push(theGladiator.id);
		horor.items.push(euroTrip.id);
		comedy.items.push(theGladiator.id);
		comedy.items.push(euroTrip.id);
		comedy.items.push(theBigLebowski.id);
		
		it("Save all", function(done) {
			tags.save(drama, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);
				
				tags.save(horor, function(err, result) {
					Should.not.exist(err);
					Should.exist(result);
					
					tags.save(comedy, function(err, result) {
						Should.not.exist(err);
						Should.exist(result);
						
						items.save(theBigLebowski, function(err, result) {
							Should.not.exist(err);
							Should.exist(result);
							
							items.save(theGladiator, function(err, result) {
								Should.not.exist(err);
								Should.exist(result);
								
								items.save(euroTrip, function(err, result) {
									Should.not.exist(err);
									Should.exist(result);
									done();
								});
							});
						});
					});
				});
			});
		});
		
		it("Load theGladiator", function(done) {
			items.load(theGladiator.id, { referencesDepth:1 }, function(err, result) {
				Should.not.exist(err);
				Should.exist(result);

				result.id.should.equal(theGladiator.id);
				result.name.should.equal(theGladiator.name);
				Object.keys(result.tags).length.should.equal(theGladiator.tags.length)
				
				result.tags[1].should.containDeep(drama);
				result.tags[2].should.containDeep(horor);
				result.tags[3].should.containDeep(comedy);
				
				done();
			});
		});
	});
});
