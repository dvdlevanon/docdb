var Assert 		= require("assert");
var Should 		= require("should");
var Async 		= require("async");
var FileSystem 	= require("fs-extra");
var OS 			= require("os");
var Path		= require("path");
var Database 	= require("../src/Database");
var DBMap 		= require("../src/types/DBMap");

describe("DBMap", function() {
	var dbpath = Path.join(OS.tmpdir(), "docdb-test/test-db-map");
	FileSystem.emptyDirSync(dbpath);
	var db = new Database(dbpath);
	
	describe("Map.put(value)", function() {
		var map = new DBMap(db, "put-map");
		
		it("Adding to map", function(done) {
			map.put("my key", "some value", function(err) {
				Should.not.exist(err);
				
				map.exists("my key", function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					done();
				});
			});
		});
		
		it("Adding to batch", function(done) {
			var operations = [];
			map.putBatch("david", "levanon", operations);
			
			db.batch(operations, function(err) {
				map.exists("david", function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					done();
				});
			});
		});
		
		it("Adding multiple to batch", function(done) {
			var operations = [];
			map.putBatch("many.mult.1", "val1", operations);
			map.putBatch("many.mult.2", "val2", operations);
			
			db.batch(operations, function(err) {
				map.exists("many.mult.1", function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					
					map.exists("many.mult.2", function(err, exists) {
						Should.not.exist(err);
						exists.should.equal(true);
						done();
					});
				});
			});
		});
		
		it("Putting multiple values", function(done) {
			var entries = [
				{ key:"key1", value: "string key" },
				{ key:123, value: "number key" },
				{ key:true, value: "boolean key" }
			];
			
			Async.each(entries, 
				function add(entry, callback) {
					map.put(entry.key, entry.value, function(err) {
						callback(err);
					});
				},
				function finish(err) {
					Should.not.exist(err);
					
					Async.each(entries, 
						function exists(entry, callback) {
							map.exists(entry.key, function(err, exists) {
								Should.not.exist(err);
								exists.should.equal(true);
								callback(err);
							});
						},
						function finish(err) {
							Should.not.exist(err);
							done();
						}
					);
				}
			);
		});
	});
	
	describe("Map.exists(value)", function() {
		var map = new DBMap(db, "exists-map");
		
		it("Test existing value", function(done) {
			map.put("exists.key", "value", function(err) {
				Should.not.exist(err);
				
				map.exists("exists.key", function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					done();
				});
			});
		});
		
		it("Test not existing value", function(done) {
			map.exists("not.exists.key", function(err, exists) {
				Should.not.exist(err);
				exists.should.equal(false);
				done();
			});
		});
	});
	
	describe("Map.delete(value)", function() {
		var map = new DBMap(db, "delete-map");
		
		it("Delete existing value", function(done) {
			map.put("delete.key", "some value", function(err) {
				Should.not.exist(err);
				
				map.exists("delete.key", function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					
					map.delete("delete.key", function(err) {
						Should.not.exist(err);
						
						map.exists("delete.key", function(err, exists) {
							Should.not.exist(err);
							exists.should.equal(false);
							done();
						});
					});
				});
			});
		});
		
		it("Delete not existing value", function(done) {
			map.delete("delete.not.exists.key", function(err) {
				Should.not.exist(err);
				done();
			});
		});
	});
	
	describe("Map.list()", function() {
		var map = new DBMap(db, "list-map");
		
		it("List without values", function(done) {
			map.list(function(err, entries) {
				Should.not.exist(err);
				Should.exist(entries);
				Object.keys(entries).should.have.length(0);
				done();
			});
		});
		
		it("List one values", function(done) {
			map.put(1, "test.list.one", function(err) {
				Should.not.exist(err);
				
				map.list(function(err, entries) {
					Should.not.exist(err);
					Should.exist(entries);
					Object.keys(entries).should.have.length(1);
					entries[1].should.equal("test.list.one");
					done();
				});
			});
		});
		
		it("List multiple values", function(done) {
			var mapMultiple = new DBMap(db, "list-map-multi");
			var entries = [
				{ key: "name", value: "david" },
				{ key: "address", value: "kfar sava" },
				{ key: "age", value: 28 },
				{ key: "male", value: true },
				{ key: "children", value: {
					"first": "orit",
					"second": "daniel"
				} },
				{ key: "some-array", value: ["1", "2", "3"] }
			];
			
			Async.each(entries, 
				function add(entry, callback) {
					mapMultiple.put(entry.key, entry.value, function(err) {
						callback(err);
					});
				},
				function finish(err) {
					Should.not.exist(err);
					
					mapMultiple.list(function(err, result) {
						Should.not.exist(err);
						Should.exist(result);
						Object.keys(result).length.should.equal(entries.length);
						
						for (var i = 0; i < entries.length; i++) {
							var resultValue = result[entries[i].key];
							Should.exist(resultValue);
							resultValue.should.eql(entries[i].value);
						}
						
						done();
					});
				}
			);
		});
	});
});
