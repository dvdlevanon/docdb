var Assert 		= require("assert");
var Should 		= require("should");
var Async 		= require("async");
var FileSystem 	= require("fs-extra");
var OS 			= require("os");
var Path		= require("path");
var Database 	= require("../src/Database");
var DBSet 		= require("../src/types/DBSet");

describe("DBSet", function() {
	var dbpath = Path.join(OS.tmpdir(), "docdb-test/test-db-set");
	FileSystem.emptyDirSync(dbpath);
	var db = new Database(dbpath);
	
	describe("Set.put(value)", function() {
		var set = new DBSet(db, "add-set");
		
		it("Adding to set", function(done) {
			set.put(1, function(err) {
				Should.not.exist(err);
				
				set.exists(1, function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					done();
				});
			});
		});
		
		it("Adding to batch", function(done) {
			var operations = [];
			set.putBatch(2, operations);
			
			db.batch(operations, function(err) {
				set.exists(2, function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					done();
				});
			});
		});
		
		it("Adding multiple values", function(done) {
			var values = [10, 11, 12, 13, 14, 15];
			
			Async.each(values, 
				function add(value, callback) {
					set.put(value, function(err) {
						callback(err);
					});
				},
				function finish(err) {
					Should.not.exist(err);
					
					Async.each(values, 
						function exists(value, callback) {
							set.exists(value, function(err, exists) {
								Should.not.exist(err);
								exists.should.equal(true);
								callback(err);
							});
						},
						function finish(err) {
							Should.not.exist(err);
							done();
						}
					);
				}
			);
		});
	});
	
	describe("Set.exists(value)", function() {
		var set = new DBSet(db, "exists-set");
		
		it("Test existing value", function(done) {
			set.put(1, function(err) {
				Should.not.exist(err);
				
				set.exists(1, function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					done();
				});
			});
		});
		
		it("Test not existing value", function(done) {
			set.exists(2, function(err, exists) {
				Should.not.exist(err);
				exists.should.equal(false);
				done();
			});
		});
	});
	
	describe("Set.delete(value)", function() {
		var set = new DBSet(db, "delete-set");
		
		it("Delete existing value", function(done) {
			set.put(1, function(err) {
				Should.not.exist(err);
				
				set.exists(1, function(err, exists) {
					Should.not.exist(err);
					exists.should.equal(true);
					
					set.delete(1, function(err) {
						Should.not.exist(err);
						
						set.exists(1, function(err, exists) {
							Should.not.exist(err);
							exists.should.equal(false);
							done();
						});
					});
				});
			});
		});
		
		it("Delete not existing value", function(done) {
			set.delete(2, function(err) {
				Should.not.exist(err);
				done();
			});
		});
	});
	
	describe("Set.list()", function() {
		var set = new DBSet(db, "list-set");
		
		it("List without values", function(done) {
			set.list(function(err, values) {
				Should.not.exist(err);
				Should.exist(values);
				values.should.have.length(0);
				done();
			});
		});
		
		it("List one values", function(done) {
			set.put(1, function(err) {
				Should.not.exist(err);
				
				set.list(function(err, values) {
					Should.not.exist(err);
					Should.exist(values);
					values.should.have.length(1);
					values[0].should.equal(1);
					done();
				});
			});
		});
		
		it("List multiple values", function(done) {
			var setMultiple = new DBSet(db, "list-set-multi");
			var values = ["david fdsaf sadf sfkjh da sdkjfha sdkjfhsdjk hdfkjsadhdfkjsda hflsadf", "levanon", "rachel", "cohen", "love", "forever"];
			
			Async.each(values, 
				function add(value, callback) {
					setMultiple.put(value, function(err) {
						callback(err);
					});
				},
				function finish(err) {
					Should.not.exist(err);
					
					setMultiple.list(function(err, result) {
						Should.not.exist(err);
						Should.exist(result);
						result.should.have.length(values.length);
						result.sort().should.eql(values.sort());
						done();
					});
				}
			);
		});
	});
});
