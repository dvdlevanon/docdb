var Should 		= require("should");
var FileSystem 	= require("fs-extra");
var OS 			= require("os");
var Path		= require("path");
var Database 	= require("../src/Database");

describe("Database", function() {
	var dbpath = Path.join(OS.tmpdir(), "docdb-test/test-db-db");
	FileSystem.emptyDirSync(dbpath);
	var db = new Database(dbpath);
	
	describe("db.put(key, value)", function() {
		it("Put new something", function(done) {
			db.put("db.put.test-key", "test-value", function(err) {
				Should.not.exist(err);
				
				db.get("db.put.test-key", function(err, value) {
					Should.not.exist(err);
					Should.exist(value);
					value.should.equal("test-value");
					done();
				})
			});
		});
		
		it("Put the same key twice", function(done) {
			db.put("db.put.twice-key", "value-first-time", function(err) {
				Should.not.exists(err);
				
				db.get("db.put.twice-key", function(err, value) {
					Should.not.exist(err);
					Should.exist(value);
					value.should.equal("value-first-time");
				});
				
				db.put("db.put.twice-key", "value-second-time", function(err) {
					Should.not.exists(err);
					
					db.get("db.put.twice-key", function(err, value) {
						Should.not.exist(err);
						Should.exist(value);
						value.should.equal("value-second-time");
						done();
					});
				});
			});
		});
		
		it("Put binary key twice", function(done) {
			var buffer = Buffer.allocUnsafe(8);
			buffer.writeIntLE(12345678);
			
			db.put(buffer, "val1", function(err) {
				Should.not.exist(err);
				
				db.get(buffer, function(err, result) {
					Should.not.exist(err);
					Should.exist(result);
					result.should.equal("val1");
					
					db.put(buffer, "val2", function(err) {
						Should.not.exist(err);
						
						db.get(buffer, function(err, result) {
							Should.not.exist(err);
							Should.exist(result);
							result.should.equal("val2");
							done();
						});
					});
				});
			});
		});
	});
	
	describe("db.del(key)", function() {
		it("Delete key", function(done) {
			db.put("db.del-key", "test-value", function(err) {
				Should.not.exist(err);
				
				db.get("db.del-key", function(err, value) {
					Should.not.exist(err);
					Should.exist(value);
					
					db.del("db.del-key", function(err) {
						Should.not.exist(err);
						
						db.get("db.del-key", function(err, value) {
							Should.not.exist(err);
							Should.not.exist(value);
							done();
						});
					});
				});
			});
		});
	});
	
	describe("db.get(key)", function() {
		it("Get something not exists", function(done) {
			db.get("db.get.not.exists", function(err, value) {
				Should.not.exist(err);
				Should.not.exist(value);
				done();
			});
		});
		
		
		it("Get something that exists", function(done) {
			db.put("db.get.exists.key", "exists key", function(err) {
				Should.not.exist(err);
				
				db.get("db.get.exists.key", function(err, value) {
					Should.not.exist(err);
					Should.exist(value);
					value.should.equal("exists key");
					done();
				});
			})
		});
	});
	
	describe("db.batch(operations)", function() {
		it("Put some keys", function(done) {
			var operations = [];
			
			for (var i = 0; i < 100; i++) {
				operations.push({ type: "put", key: "db.batch.key" + i, value: "value" + i });
			}
			
			db.batch(operations, function(err) {
				Should.not.exists(err);
								
				db.get("db.batch.key0", function(err, value) {
					Should.not.exist(err);
					Should.exist(value);
					value.should.equal("value0");
					
					db.get("db.batch.key5", function(err, value) {
						Should.not.exist(err);
						Should.exist(value);
						value.should.equal("value5");
						
						db.get("db.batch.key99", function(err, value) {
							Should.not.exist(err);
							Should.exist(value);
							value.should.equal("value99");
							done();
						});
					});
				});
			});
		});
		
		it("Put the same keys", function(done) {
			var operations = [];
			
			operations.push({ type: "put", key: "db.batch.same-key", value: "first value" });
			operations.push({ type: "put", key: "db.batch.same-key", value: "second value" });
			operations.push({ type: "put", key: "db.batch.new-key", value: "new key" });
			operations.push({ type: "put", key: "db.batch.same-key", value: "third value" });
			
			db.batch(operations, function(err) {
				Should.not.exists(err);
				
				db.get("db.batch.same-key", function(err, value) {
					Should.not.exist(err);
					Should.exist(value);
					value.should.equal("third value");
					
					db.get("db.batch.new-key", function(err, value) {
						Should.not.exist(err);
						Should.exist(value);
						value.should.equal("new key");
						done();
					});
				});
			});
		});
	});
	
	describe("db.keys(start, end)", function() {
		it("Get renge of keys", function(done) {
			var operations = [];
			var keyPrefix = "db.get.keys.key"
			
			for (var i = 0; i < 100; i++) {
				var key = Buffer.allocUnsafe(keyPrefix.length + 4);
				key.write(keyPrefix);
				key.writeInt32LE(i, keyPrefix.length);
				
				operations.push({ type: "put", key: key, value: "value" + i });
			}
			
			var startKey = Buffer.allocUnsafe(keyPrefix.length + 4);
			startKey.write(keyPrefix);
			startKey.writeInt32LE(57, keyPrefix.length);
			
			var endKey = Buffer.allocUnsafe(keyPrefix.length + 4);
			endKey.write(keyPrefix);
			endKey.writeInt32LE(65, keyPrefix.length);
			
			db.batch(operations, function(err) {
				Should.not.exists(err);
				
				db.keys(startKey, endKey, function(err, result) {
					Should.not.exists(err);
					Should.exists(result);
					Object.keys(result).should.have.length(66 - 57);
					
					for (var i = 57; i < 66; i++) {
						var key = Buffer.allocUnsafe(keyPrefix.length + 4);
						key.write(keyPrefix);
						key.writeInt32LE(i, keyPrefix.length);
						
						Should.exists(result[key]);
						result[key].should.equal("value" + i);
					}
					
					done();
				});
			});
		});
		
		it("Binary renge with duplicate keys", function(done) {
			var operations = [];
			var keyPrefix1 = "db.get.keys.duplicate"
			var key1 = Buffer.alloc(keyPrefix1.length + 8);
			key1.write(keyPrefix1);
			key1.writeIntLE(1, keyPrefix1.length);
			
			operations.push({ type: "put", key: key1, value: "value1" });
			
			db.batch(operations, function(err) {
				Should.not.exists(err);
			
				var operations2 = [];
				var keyPrefix2 = "db.get.keys.duplicate"
				var key2 = Buffer.alloc(keyPrefix2.length + 8);
				key2.write(keyPrefix2);
				key2.writeIntLE(1, keyPrefix2.length);
				
				operations2.push({ type: "put", key: key2, value: "value2" });
				
				db.batch(operations2, function(err) {
					Should.not.exists(err);
					
					var startKey = "db.get.keys.duplicate"
					
					var endKey = Buffer.alloc(startKey.length + 8);
					endKey.write(startKey);
					endKey.writeIntLE(Number.MAX_SAFE_INTEGER, startKey.length);
					
					db.keys(startKey, endKey, function(err, result) {
						Should.not.exists(err);
						Should.exists(result);
						Object.keys(result).should.have.length(1);
						done();
					});
				});
			});
		});
	});
	
	describe("db.values(start, end)", function() {
		it("Get renge of values", function(done) {
			var operations = [];
			var keyPrefix = "db.get.keys.key"
			
			for (var i = 0; i < 100; i++) {
				var key = Buffer.allocUnsafe(keyPrefix.length + 4);
				key.write(keyPrefix);
				key.writeInt32LE(i, keyPrefix.length);
				
				operations.push({ type: "put", key: key, value: "value" + i });
			}
			
			var startKey = Buffer.allocUnsafe(keyPrefix.length + 4);
			startKey.write(keyPrefix);
			startKey.writeInt32LE(57, keyPrefix.length);
			
			var endKey = Buffer.allocUnsafe(keyPrefix.length + 4);
			endKey.write(keyPrefix);
			endKey.writeInt32LE(65, keyPrefix.length);
			
			db.batch(operations, function(err) {
				Should.not.exists(err);
				
				db.values(startKey, endKey, function(err, result) {
					Should.not.exists(err);
					Should.exists(result);
					result.should.have.length(66 - 57);
					
					for (var i = 57; i < 66; i++) {
						var value = "value" + i;
						result.should.containEql(value);
					}
					
					done();
				});
			});
		});
	});
});
