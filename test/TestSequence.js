var Should 		= require("should");
var OS 			= require("os");
var Path		= require("path");
var FileSystem 	= require("fs-extra");
var Database 	= require("../src/Database");
var DBSequence 	= require("../src/types/DBSequence");

describe("DBSequence", function() {
	var dbpath = Path.join(OS.tmpdir(), "docdb-test/test-db-seq");
	FileSystem.emptyDirSync(dbpath);
	var db = new Database(dbpath);
	var seq1 = new DBSequence(db, "seq1")
	
	describe("DBSequence.next()", function() {
		var existingValues = {}
		
		it("Next 10 values", function(done) {
			seq1.next(function(err, value) {
				Should.not.exist(err);
				Should.exist(value);
				Should.not.exist(existingValues[value]);
				existingValues[value] = value;
				done();
			});
		});
	});
});
